class Employee {
   constructor(name, age, salary) {
       this.name = name;
       this.age = age;
       this.salary = salary;
   }
   get userName() { return this.name; }
   set userName(value) { this.name = value; }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }
    get userSalary() { return this.salary * 3 }
}

const nikita = new Programmer("nikita", 18, 50000, "js")



